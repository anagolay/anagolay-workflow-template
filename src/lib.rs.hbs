{{! START Dependencies for nostd Workflow ---------------------------------------------- }}
{{#unless std}}
#![cfg_attr(not(feature = "std"), no_std)]

#[cfg(not(feature = "std"))]
extern crate alloc;
#[cfg(not(feature = "std"))]
use alloc::{
  boxed::Box,
  collections::BTreeMap,
  rc::Rc,
  string::{String, ToString},
  vec::Vec,
};
{{/unless}}
{{! END Dependencies for nostd Workflow ---------------------------------------------- }}

{{! START Dependencies for std Workflow ------------------------------------------------ }}
#[cfg(all(feature = "debug_assertions", feature = "js", target_arch = "wasm32"))]
use console_error_panic_hook;
#[cfg(all(feature = "debug_assertions", feature = "js", target_arch = "wasm32"))]
use std::panic;
{{! END Dependencies for std Workflow -------------------------------------------------- }}

{{! START Common dependencies -------------------------------------------------------- }}
#[cfg(feature = "std")]
use std::{boxed::Box, collections::BTreeMap, rc::Rc};

{{! Bring the few, simple types of an_operation support in scope (RcAny, Bytes, GenericId...) }}
use an_operation_support::{*, workflow::RcAny};

#[cfg(any(feature = "js", feature = "std"))]
use instant::Instant;
#[cfg(all(feature = "js", target_arch = "wasm32"))]
use wasm_bindgen::{prelude::wasm_bindgen, JsValue};
#[cfg(all(feature = "js", target_arch = "wasm32"))]
use serde::{Serialize, Deserialize};

{{! Bring the Operation packages into scope, so that we can refer to their own defined types }}
{{~#each operations as |operation|~}}
use {{snakeCase this.data.name}};
{{/each}}
{{! END Common dependencies ---------------------------------------------------------- }}


{{! START Segment execute functions: their implementation produces the concatenation of 
    the Operations for every Segment of the Workflow
    ---------------------------------------------------------------------------------- }}
{{#each manifestData.segments as |segment segmentIndex|}}
/// Generated
/// - Accept exact input parameter type and cardinality
/// - Config of each operation in segment is known from workflow manifest
/// - Produces a Rc wrapped output in a Result
{{! The function parameters are the inputs of the first Operation of the Segment, 
    named incrementally: input_0, input_1...
    The output type instead comes from the last Operation of the Segment}}
async fn execute_segment_{{segmentIndex}}(
  {{#each sequence as |opVerRef|~}}
    {{#if @first}}
      {{#each (lookup @root.operationsLookup segmentIndex) }}
        {{#if (eq this.verId opVerRef.versionId)}}
          {{#if (hasProperty opVerRef "inputs" )}}
            {{#each opVerRef.inputs as |opInput opInputIndex|}}
  input_{{opInputIndex}}: &{{opInput}}
            {{/each}}
          {{else}}
            {{#each this.op.data.inputs as |opInput opInputIndex|}}
  input_{{opInputIndex}}: &{{opInput}}
            {{/each}}
          {{/if}}
        {{/if}}
      {{/each}}
    {{/if}}
  {{/each}}
) -> Result<(usize, {{#each sequence as |opVerRef|~}}
    {{#if @last}}
      {{#each (lookup @root.operationsLookup segmentIndex) }}
        {{~#if (eq this.verId opVerRef.versionId)~}}
          {{~#if (hasProperty opVerRef "output" )~}}
Rc<{{opVerRef.output}}>
          {{~else~}}
Rc<{{this.op.data.output}}>
          {{~/if~}}
        {{~/if~}}
      {{/each}}
    {{/if}}
  {{~/each~}}), String> {
  #[cfg(any(feature = "js", feature = "std"))]
  let now = Instant::now();

  {{! Construct the Operations of the Segment, passing the output of the previous to the input of the next.
      In this moment, the config from the manifest is stored into a BTreeMap which is passed on to the execute() function
      Only the first iteration is slightly different because the Operation accepts the function arguments in input }}
  {{#each sequence as |opVerRef|}}
    {{#each (lookup @root.operationsLookup segmentIndex) }}
      {{#if (eq this.verId opVerRef.versionId)}}
  let config = BTreeMap::from([
                 {{#each opVerRef.config}}("{{@key}}".to_string(), "{{this}}".to_string()){{#unless @last}}, {{/unless}}{{/each}}
               ]);
      {{/if}}
    {{/each}}
    {{#if @first}}
      {{#each (lookup @root.operationsLookup segmentIndex) }}
        {{#if (eq this.verId opVerRef.versionId)}}
            {{#if (contains this.op.data.groups "FLOWCONTROL")}}
  let output = {{snakeCase this.op.data.name}}::execute!(
  {{~#if (hasProperty opVerRef "inputs" )~}}
    {{~#each opVerRef.inputs~}}
        input_{{@index}}{{~#unless @last~}}, {{~/unless~}}
    {{~/each~}} 
  {{else}}
    {{~#each this.op.data.inputs~}}
        input_{{@index}}{{~#unless @last~}}, {{~/unless~}}
    {{~/each~}}
  {{~/if~}}; config).clone();
            {{else}}
  let output = {{snakeCase this.op.data.name}}::execute(
  {{~#if (hasProperty opVerRef "inputs" )~}}
    {{~#each opVerRef.inputs~}}
        input_{{@index}}{{~#unless @last~}}, {{~/unless~}}
    {{~/each~}} 
  {{else}}
    {{~#each this.op.data.inputs~}}
        input_{{@index}}{{~#unless @last~}}, {{~/unless~}}
    {{~/each~}}
  {{~/if~}}, config)
              .await
              .map_err(|error| error.to_string())?;
            {{/if}}
        {{/if}}
      {{/each}}
    {{else}}
      {{#each (lookup @root.operationsLookup segmentIndex) }}
        {{#if (eq this.verId opVerRef.versionId)}}
           {{#if (contains this.op.data.groups "FLOWCONTROL")}}
  let output = {{snakeCase this.op.data.name}}::execute!(&output, config).clone();
           {{else}}
  let output = {{snakeCase this.op.data.name}}::execute(&output, config)
          .await
          .map_err(|error| error.to_string())?;
           {{/if}}
        {{/if}}
      {{/each}}
    {{/if}}
  {{/each}}

  {{! Wrap in a new Rc the last Operation output and return this along with some performance metric }}
  #[cfg(any(feature = "js", feature = "std"))]
  return Ok((now.elapsed().as_millis() as usize, Rc::new(output)));
  #[cfg(all(not(feature = "js"), not(feature = "std")))]
  return Ok((0 as usize, Rc::new(output)));
}
{{/each}}
{{! END Segment execution function --------------------------------------------------- }}

{{! START Segment next functions: their implementation produces the function that 
    invokes the execution of the next Segment in the Workflow.
    We need to produce two of them, one for js target and the other for native.
    It takes care of providing input from previously executed segment (saved in 
    WorkflowState) or from the operation_inputs parameter according to the value 
    indicated in the Segment definition. Infact, the "inputs" field of such structure
    is used to determine the routing of inputs 
    ---------------------------------------------------------------------------------- }}
{{#each manifestData.segments as |segment segmentIndex|}}
/// Generated for js target
/// - Retrieve input either from operation_inputs or previous segments execution result.
/// - Return a serde result. Only provide output on last segment to avoid clone
/// - If no external input are required, ignore operation_inputs parameter
#[cfg(all(feature = "js", target_arch = "wasm32"))]
async fn next_segment_{{segmentIndex}}(
  mut state: &mut WorkflowState,
  {{#each sequence as |opVerRef|}}
    {{#if @first}}
      {{#each (lookup @root.operationsLookup segmentIndex) }}
        {{#if (eq this.verId opVerRef.versionId)}}
          {{#if (contains segment.inputs -1)}}
  operation_inputs: &Vec<JsValue>,
          {{else}}
  _operation_inputs: &Vec<JsValue>,
          {{/if}}
        {{/if}}
      {{/each}}
    {{/if}}
  {{/each}}
) -> Result<JsValue, JsValue> {
  {{#each sequence as |opVerRef|}}
    {{#if @first}}
      {{#each (lookup @root.operationsLookup segmentIndex) }}
        {{#if (eq this.verId opVerRef.versionId)}}
          {{#if (contains this.op.data.groups "FLOWCONTROL")}}
            {{#each opVerRef.inputs as |opInput opInputIndex|}}
              {{#each segment.inputs as |segInput segInputIndex|}} 
                {{#if (eq segInputIndex opInputIndex)}}
                  {{#if (ne -1 segInput)}}
  let input_{{opInputIndex}} = state.segment_results.get({{segInput}}).unwrap();
  let input_{{opInputIndex}}: &{{opInput}} = input_{{opInputIndex}}.downcast_ref::<{{opInput}}>().unwrap();
                  {{else}}
  let input_{{opInputIndex}} = operation_inputs.get({{opInputIndex}}).unwrap();
                    {{#if (eq opInput "Bytes")}}
  let input_{{opInputIndex}}: Bytes = from_bytes(input_{{opInputIndex}})?;
                    {{else}}
  let input_{{opInputIndex}}: {{opInput}} = from_value(input_{{opInputIndex}})?;
                    {{/if}}
                  {{/if}}
                {{/if}}
              {{/each}}
            {{/each}}
          {{else}}
            {{#each this.op.data.inputs as |opInput opInputIndex|}}
              {{#each segment.inputs as |segInput segInputIndex|}} 
                {{#if (eq segInputIndex opInputIndex)}}
                  {{#if (ne -1 segInput)}}
  let input_{{opInputIndex}} = state.segment_results.get({{segInput}}).unwrap();
  let input_{{opInputIndex}}: &{{opInput}} = input_{{opInputIndex}}.downcast_ref::<{{opInput}}>().unwrap();
                  {{else}}
  let input_{{opInputIndex}} = operation_inputs.get({{opInputIndex}}).unwrap();
                    {{#if (eq opInput "Bytes")}}
  let input_{{opInputIndex}}: Bytes = from_bytes(input_{{opInputIndex}})?;
                    {{else}}
  let input_{{opInputIndex}}: {{opInput}} = from_value(input_{{opInputIndex}})?;
                    {{/if}}
                  {{/if}}
                {{/if}}
              {{/each}}
            {{/each}}
          {{/if}}
        {{/if}}
      {{/each}}
    {{/if}}
  {{/each}}

{{! Once inputs are ready, the respective execute_segment_x function is called. Its output is used to produce a structure of type
    SegmentXResult. }}
  let segment_execution = execute_segment_{{segmentIndex}}({{#each segment.inputs}}{{#unless (ne -1 this)}}&{{/unless}}input_{{@index}}{{~#unless @last~}}, {{~/unless~}}{{~/each~}})
    .await
    .map_err(|error_msg| to_value(&error_msg).unwrap())?;
  state.total_time += segment_execution.0;
  state
    .segment_results
    .insert({{segmentIndex}}, segment_execution.1.clone());
  let segment_result = to_value(&Segment{{segmentIndex}}Result {
    done: {{#if @last}}true{{else}}false{{/if}},
    {{#if @last}}
    output: segment_execution.1.as_ref().clone(),
    {{/if}}
    segment_time: segment_execution.0,
    total_time: state.total_time,
  })?;
  state.segment_index += 1;
  Ok(segment_result)
}
{{/each}}

{{#each manifestData.segments as |segment segmentIndex|}}
/// Generated for native target
/// - Retrieve input either from operation_inputs or previous segments execution result.
/// - Downcast Any to the expected input type
/// - Return a boxed result. Only provide output on last segment to avoid clone
/// - If no external input are required, ignore operation_inputs parameter
#[cfg(not(feature = "js"))]
async fn next_segment_{{segmentIndex}}(
  mut state: &mut WorkflowState,
  {{#each sequence as |opVerRef|}}
    {{#if @first}}
      {{#each (lookup @root.operationsLookup segmentIndex) }}
        {{#if (eq this.verId opVerRef.versionId)}}
          {{#if (contains segment.inputs -1)}}
  operation_inputs: &Vec<RcAny>,
          {{else}}
  _operation_inputs: &Vec<RcAny>,
          {{/if}}
        {{/if}}
      {{/each}}
    {{/if}}
  {{/each}}
) -> Result<Box<dyn workflow::SegmentResult>, String> {
  {{#each sequence as |opVerRef|}}
    {{#if @first}}
      {{#each (lookup @root.operationsLookup segmentIndex) }}
        {{#if (eq this.verId opVerRef.versionId)}}
          {{#if (contains this.op.data.groups "FLOWCONTROL")}}
            {{#each opVerRef.inputs as |opInput opInputIndex|}}
              {{#each segment.inputs as |segInput segInputIndex|}} 
                {{#if (eq segInputIndex opInputIndex)}}
                  {{#if (ne -1 segInput)}}
  let input_{{opInputIndex}} = state.segment_results.get({{segInput}}).unwrap();
                  {{else}}
  let input_{{opInputIndex}} = operation_inputs.get({{opInputIndex}}).unwrap();
                  {{/if}}
  let input_{{opInputIndex}}: &{{opInput}} = input_{{opInputIndex}}.downcast_ref::<{{opInput}}>().unwrap();
                {{/if}}
              {{/each}}
            {{/each}}
          {{else}}
            {{#each this.op.data.inputs as |opInput opInputIndex|}}
              {{#each segment.inputs as |segInput segInputIndex|}} 
                {{#if (eq segInputIndex opInputIndex)}}
                  {{#if (ne -1 segInput)}}
  let input_{{opInputIndex}} = state.segment_results.get({{segInput}}).unwrap();
                  {{else}}
  let input_{{opInputIndex}} = operation_inputs.get({{opInputIndex}}).unwrap();
                  {{/if}}
  let input_{{opInputIndex}}: &{{opInput}} = input_{{opInputIndex}}.downcast_ref::<{{opInput}}>().unwrap();
                {{/if}}
              {{/each}}
            {{/each}}         
          {{/if}}
        {{/if}}
      {{/each}}
    {{/if}}
  {{/each}}

{{! Once inputs are ready, the respective execute_segment_* function is called. Its output is used to produce a structure of type
    Segment*Result }}
  let segment_execution = execute_segment_{{segmentIndex}}({{#each segment.inputs}}&input_{{@index}}{{~#unless @last~}}, {{~/unless~}}{{~/each~}}).await?;
  state.total_time += segment_execution.0;
  state
    .segment_results
    .insert({{segmentIndex}}, segment_execution.1.clone());
  let segment_result = Box::from(Segment{{segmentIndex}}Result {
    done: {{#if @last}}true{{else}}false{{/if}},
    {{#if @last}}
    output: segment_execution.1.clone(),
    {{/if}}
    segment_time: segment_execution.0,
    total_time: state.total_time,
  });
  state.segment_index += 1;
  Ok(segment_result)
}
{{/each}}
{{! END Segment next function ------------------------------------------------------- }}

{{! START Segment result types: for each segment, produce a structure of type 
    Segment*Result. It will be used as return type for the next_segment_* function and 
    will contain an output only if it's the final Workflow segment, to preserve 
    performance. Moreover, in this case there is a difference between the wasm-bound 
    object, that requires a cloned output and the native one, which uses a reference
    --------------------------------------------------------------------------------- }}
{{#each manifestData.segments as |segment segmentIndex|}}
  {{~#if @last}}
/// Generated for js target
#[cfg(all(feature = "js", target_arch = "wasm32"))]
#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Segment{{segmentIndex}}Result {
  done: bool,
  output: {{#each sequence as |opVerRef|~}}
                {{~#if @last~}}
                  {{~#each (lookup @root.operationsLookup segmentIndex) ~}}
                    {{~#if (eq this.verId opVerRef.versionId)~}}
                      {{this.op.data.output}},
                    {{/if}}
                  {{/each}}
                {{/if}}
              {{/each}}
  segment_time: usize,
  total_time: usize,
}

/// Generated for native target
#[cfg(not(feature = "js"))]
pub struct Segment{{segmentIndex}}Result {
  done: bool,
  output: {{#each sequence as |opVerRef|~}}
                {{~#if @last~}}
                  {{~#each (lookup @root.operationsLookup segmentIndex) ~}}
                    {{~#if (eq this.verId opVerRef.versionId)~}}
                      Rc<{{this.op.data.output}}>,
                    {{/if}}
                  {{/each}}
                {{/if}}
              {{/each}}
  segment_time: usize,
  total_time: usize,
}

#[cfg(not(feature = "js"))]
impl workflow::SegmentResult for Segment{{segmentIndex}}Result {
    fn is_done(&self) -> bool {
        self.done
    }

    fn get_output(&self) -> Option<RcAny> {
        Some(self.output.clone())
    }

    fn get_segment_time(&self) -> usize {
        self.segment_time
    }

    fn get_total_time(&self) -> usize {
        self.total_time
    }
}
  {{else}}
/// Generated for js target
#[cfg(all(feature = "js", target_arch = "wasm32"))]
#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Segment{{segmentIndex}}Result {
  done: bool,
  segment_time: usize,
  total_time: usize,
}

/// Generated for native target
#[cfg(not(feature = "js"))]
pub struct Segment{{segmentIndex}}Result {
  done: bool,
  segment_time: usize,
  total_time: usize,
}

#[cfg(not(feature = "js"))]
impl workflow::SegmentResult for Segment{{segmentIndex}}Result {
    fn is_done(&self) -> bool {
        self.done
    }

    fn get_output(&self) -> Option<RcAny> {
        None
    }

    fn get_segment_time(&self) -> usize {
        self.segment_time
    }

    fn get_total_time(&self) -> usize {
        self.total_time
    }
}
  {{/if}}
{{/each}}
{{! END Segment result types ------------------------------------------------------------ }}

{{! START Segment match: a router that choses what Segment to execute next according to 
    the last executed segment_index, stored in the WorkflowState. When the next segment does
    not contains external input, chain it directly
    ------------------------------------------------------------------------------------- }}
/// Generated for js target
#[cfg(all(feature = "js", target_arch = "wasm32"))]
async fn match_segments(
  state: &mut WorkflowState,
  operation_inputs: Vec<JsValue>,
) -> Result<JsValue, JsValue> {
  loop {
    match state.segment_index {
      {{#each manifestData.segments as |segment segmentIndex|}}
      {{@index}} => {
        {{#if (ne true @last) }}
          {{! If next segment does not require external input and this does, pass it to this and keep looping }}
          {{#if (and (eq false (contains (lookup (lookup @root.manifestData.segments (add @index 1)) "inputs") -1))
                     (eq true (contains (lookup segment "inputs") -1))
                 ) }}
          next_segment_{{@index}}(state, &operation_inputs).await?;
          {{/if}}
          {{! If next segment does not require external input and this doesnt either, pass none to this and keep looping }}
          {{#if (and (eq false (contains (lookup (lookup @root.manifestData.segments (add @index 1)) "inputs") -1))
                     (eq false (contains (lookup segment "inputs") -1))
                 ) }}
          next_segment_{{@index}}(state, &Vec::new()).await?;
          {{/if}}
          {{! If this segment does require external input and next does either, pass it to this and break }}
          {{#if (and (eq true (contains (lookup (lookup @root.manifestData.segments (add @index 1)) "inputs") -1))
                     (eq true (contains (lookup segment "inputs") -1))
                 ) }}
          break Ok(next_segment_{{@index}}(state, &operation_inputs).await?)
          {{/if}}
          {{! If this segment does not require external input and next does, pass none to this and break }}
          {{#if (and (eq true (contains (lookup (lookup @root.manifestData.segments (add @index 1)) "inputs") -1))
                     (eq false (contains (lookup segment "inputs") -1))
                 ) }}
          break Ok(next_segment_{{@index}}(state, &Vec::new()).await?)
          {{/if}}
        {{else}}  
          {{! Case of last segment require external input, pass it and break }}
          {{#if (eq true (contains (lookup segment "inputs") -1)) }}
          break Ok(next_segment_{{@index}}(state, &operation_inputs).await?)
          {{/if}}
          {{! Case of last segment does not require external input, pass none and break }}
          {{#if (eq false (contains (lookup segment "inputs") -1)) }}
          break Ok(next_segment_{{@index}}(state, &Vec::new()).await?)
          {{/if}}
        {{/if}}
      },
      {{/each}}
      _ => break Err(JsValue::from_str("Workflow terminated")),
    }
  }
}

/// Generated for native target
#[cfg(not(feature = "js"))]
async fn match_segments(
  state: &mut WorkflowState,
  operation_inputs: Vec<RcAny>,
) -> Result<Box<dyn workflow::SegmentResult>, String> {
  loop {
    match state.segment_index {
      {{#each manifestData.segments as |segment segmentIndex|}}
      {{@index}} => {
        {{#if (ne true @last) }}
          {{! If next segment does not require external input and this does, pass it to this and keep looping }}
          {{#if (and (eq false (contains (lookup (lookup @root.manifestData.segments (add @index 1)) "inputs") -1))
                     (eq true (contains (lookup segment "inputs") -1))
                 ) }}
          next_segment_{{@index}}(state, &operation_inputs).await?;
          {{/if}}
          {{! If next segment does not require external input and this doesnt either, pass none to this and keep looping }}
          {{#if (and (eq false (contains (lookup (lookup @root.manifestData.segments (add @index 1)) "inputs") -1))
                     (eq false (contains (lookup segment "inputs") -1))
                 ) }}
          next_segment_{{@index}}(state, &Vec::new()).await?;
          {{/if}}
          {{! If this segment does require external input and next does either, pass it to this and break }}
          {{#if (and (eq true (contains (lookup (lookup @root.manifestData.segments (add @index 1)) "inputs") -1))
                     (eq true (contains (lookup segment "inputs") -1))
                 ) }}
          break Ok(next_segment_{{@index}}(state, &operation_inputs).await?)
          {{/if}}
          {{! If this segment does not require external input and next does, pass none to this and break }}
          {{#if (and (eq true (contains (lookup (lookup @root.manifestData.segments (add @index 1)) "inputs") -1))
                     (eq false (contains (lookup segment "inputs") -1))
                 ) }}
          break Ok(next_segment_{{@index}}(state, &Vec::new()).await?)
          {{/if}}
        {{else}}  
          {{! Case of last segment require external input, pass it and break }}
          {{#if (eq true (contains (lookup segment "inputs") -1)) }}
          break Ok(next_segment_{{@index}}(state, &operation_inputs).await?)
          {{/if}}
          {{! Case of last segment does not require external input, pass none and break }}
          {{#if (eq false (contains (lookup segment "inputs") -1)) }}
          break Ok(next_segment_{{@index}}(state, &Vec::new()).await?)
          {{/if}}
        {{/if}}
      },
      {{/each}}
      _ => break Err("Workflow terminated".to_string()),
    }
  }
}
{{! END Segment match -------------------------------------------------------------------- }}

{{! START Workflow implementation: consists in a struct carrying the state of segment
    executions and the async implementation of the Workflow trait 
    ------------------------------------------------------------------------------------- }}
{{! Following code is the implementation of the Workflow state for wasm and native targets}}
struct WorkflowState {
    segment_index: usize,
    segment_results: Vec<RcAny>,
    total_time: usize,
}

#[cfg_attr(all(feature = "js", target_arch = "wasm32"), wasm_bindgen)]
pub struct Workflow {
  state: WorkflowState,
}

#[cfg_attr(all(feature = "js", target_arch = "wasm32"), wasm_bindgen)]
impl Workflow {
  #[cfg_attr(all(feature = "js", target_arch = "wasm32"), wasm_bindgen(constructor))]
  pub fn new() -> Workflow {
    Workflow {
      state: WorkflowState {
        segment_index: 0,
        segment_results: Vec::new(),
        total_time: 0,
      },
    }
  }
  
  #[cfg(all(feature = "async", not(feature = "js")))]
  pub async fn next(
      mut self,
      operation_inputs: Vec<RcAny>,
  ) -> Result<Box<dyn workflow::SegmentResult>, String> {
    match_segments(&mut self.state, operation_inputs).await
  }
  
  #[cfg(all(not(feature = "async"), not(feature = "js")))]
  pub fn next(mut self, operation_inputs: Vec<RcAny>) -> Result<Box<dyn workflow::SegmentResult>, String> {
    let future = match_segments(&mut self.state, operation_inputs);
    cassette::pin_mut!(future);
    let mut cm = cassette::Cassette::new(future);
    loop {
      if let Some(x) = cm.poll_on() {
        break x;
      }
    }
  }

  #[cfg(all(feature = "async", feature = "js", target_arch = "wasm32"))]
  pub async fn next(mut self, operation_inputs: Vec<JsValue>) -> Result<JsValue, JsValue> {
    #[cfg(all(feature = "debug_assertions", feature = "js", target_arch = "wasm32"))]
    panic::set_hook(Box::new(console_error_panic_hook::hook));
    match_segments(&mut self.state, operation_inputs).await
  }
  
  #[cfg(all(not(feature = "async"), feature = "js", target_arch = "wasm32"))]
  pub fn next(mut self, operation_inputs: Vec<JsValue>) -> Result<JsValue, JsValue> {
    #[cfg(all(feature = "debug_assertions", feature = "js", target_arch = "wasm32"))]
    panic::set_hook(Box::new(console_error_panic_hook::hook));
    let future = match_segments(&mut self.state, operation_inputs);
    cassette::pin_mut!(future);
    let mut cm = cassette::Cassette::new(future);
    loop {
      if let Some(x) = cm.poll_on() {
        break x;
      }
    }
  }
}

{{! This trait allows to deal with every Workflow through a trait that exposes just two methods:
    new() to construct an instance of the Workflow and next(), to return a WASM-bound or native result for 
    the execution of the next Segment in the Workflow }}
#[cfg_attr(feature = "async", async_trait::async_trait(?Send))]
impl workflow::Workflow for Workflow {
  fn new() -> Workflow {
    Workflow::new()
  }

  #[cfg(all(feature = "async", not(feature = "js")))]
  async fn next(mut self, operation_inputs: Vec<RcAny>) -> Result<Box<dyn workflow::SegmentResult>, String> {
    self.next(operation_inputs).await
  }
  
  #[cfg(all(not(feature = "async"), not(feature = "js")))]
  fn next(self, operation_inputs: Vec<RcAny>) -> Result<Box<dyn workflow::SegmentResult>, String> {
    self.next(operation_inputs)
  }
  
  #[cfg(all(feature = "async", feature = "js", target_arch = "wasm32"))]
  async fn next(mut self, operation_inputs: Vec<JsValue>) -> Result<JsValue, JsValue> {
    self.next(operation_inputs).await
  }

  #[cfg(all(not(feature = "async"), feature = "js", target_arch = "wasm32"))]
  fn next(self, operation_inputs: Vec<JsValue>) -> Result<JsValue, JsValue> {
    self.next(operation_inputs)
  }
}
{{! END Workflow implementation ------------------------------------------------------------- }}
